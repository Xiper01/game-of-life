require "curses"

class Simulation

  def initialize(filepath)
    # initialize screen to dynamically update console
    Curses.init_screen
    @win = Curses.stdscr
    read_file(filepath)
  end

  def read_file(filepath)
    file_data = File.read(filepath).split

    # get generation number. Ignore ':'
    @generation = file_data[1][0..-1].to_i

    # set height and length of the board
    @height = file_data[2].to_i
    @length = file_data[3].to_i

    # declaring board and a copy for the next generation
    @board = []
    @next_board = []

    # when new and old board are identical, the simulation end
    @stalemate = false

    fill_board(file_data[4..-1])
    print_generation
  end

  def fill_board(file_data)
    file_data.each_with_index do |row, i|
      if row.length == @length
        @board.push([])
        row.split('').each_with_index do |c, j|
          if c == '*'
            @board[i].push(true)
          elsif c == '.'
            @board[i].push(false)
          else
            raise "File format error at line #{i}, pos #{j}: unsupported character #{row[j]}. Only chars available are 'X' and '-'"
          end
        end
      else
        raise "File format error at line #{i}: line too long or too short"
      end
    end
    # clone board
    @next_board = @board.map(&:clone)
  end

  # Return char to display
  def render_cell(cell)
    if cell
      return "*"
    else
      return "."
    end
  end

  # overwrite console output
  def print_generation
    # set cursor to the top left corner
    @win.setpos(0, 0)
    @win.addstr("Generation: #{@generation}\n")
    @win.addstr("#{@height} #{@length}\n")
    @board.each do |row|
      line = ""
      row.each do |cell|
        line += render_cell(cell)
      end
      line += "\n"
      @win.addstr(line)
    end
    if @stalemate
      @win.addstr("\nStalemate, end of simulation.\nPress any key to close...")
    end
    # update screen
    @win.refresh
    sleep(0.1)
  end

  # count alive cells around
  def count_neighbors(y, x)
    neighbors = 0
    (y-1..y+1).each do |i|
      (x-1..x+1).each do |j|
        if !(i == y && j == x) && i >= 0 && i < @height && j >= 0 && j < @length
          if @board[i][j]
            neighbors += 1
          end
        end
      end
    end
    neighbors
  end

  # calculate if the cell have to change state
  def apply_rules(neighbors, alive)
    if alive
      if neighbors == 2 || neighbors == 3
        true
      else
        @stalemate = false
        false
      end
    else
      if neighbors == 3
        @stalemate = false
        true
      else
        false
      end
    end
  end

  def next_generation
    # if a cell change state, stalemate will change to false
    @stalemate = true
    @board.each_with_index do |row, i|
      row.each_with_index do |cell, j|
        @next_board[i][j] = apply_rules(count_neighbors(i, j), cell)
      end
    end
    @board = @next_board.map(&:clone)
    @generation += 1
  end

  def start_simulation
    until @stalemate
      next_generation
      print_generation
    end
    @win.getch
  end
end
